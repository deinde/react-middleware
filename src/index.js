import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reducer from './store/reducer';

import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';

//custome middleware below notice next!!!!!
const logAction = store => {
    //action is being captured by next
    return next => {
        //action i being passed in to next to move on to dispatch
        return action => {
            const result = next(action);
            console.log(`Caught in the middle ${JSON.stringify(result)}`);
            //below is allowing the action to go to be dispacthed 
            //to store
            return result
        }
    }
}

// Adding the middleware by passing to STORE!!!
const store = createStore(reducer, applyMiddleware(logAction));



ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

